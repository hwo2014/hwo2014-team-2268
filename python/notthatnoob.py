def find_next_turn(pieces, index, in_piece):
    n = len(pieces)
    current = pieces[index % n]
    if 'length' in current:
        distance = current['length'] - in_piece
        i = index + 1
        while 'length' in pieces[i % n]:
            distance += pieces[i % n]['length']
            i += 1
        return (distance, pieces[i % n])
    else:
        return (0, current)

def find_next_dangerous_turn(pieces, index, in_piece):
    n = len(pieces)
    current = pieces[index % n]
    if 'length' in current:
        distance = current['length'] - in_piece
        i = index + 1
        while 'length' in pieces[i % n] or abs(pieces[i % n]['angle']) <= 25:
            if 'length' in pieces[i % n]:
                distance += pieces[i % n]['length']
            elif 'angle' in pieces[i % n]:
                radius = pieces[i % n]['radius']
                angle = pieces[i % n]['angle']
                distance += arc_len(radius, angle)
            i += 1
        return (distance, pieces[i % n])
    else:
        return (0, current)

class NotThatBootBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.track_name = ""
		
        self.pieces = []

        self.drift_params = (0.2, 55, 0.3)
        self.slow_down_params = (0.2, 150, 0.125)
        self.safe_distance_throttle = 1.0

        self.switched_already = False
        
        self.Q = {}

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(bytes(msg + "\n", "ascii"))

    def switch_lane(self, direction):
        norm = "Left "if direction == "left" else "Right"
        return self.msg("switchLane", norm)

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self):
        return self.msg("joinRace", {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"trackName": self.track_name,
			"carCount": 1
		})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_race(self, track_name):
        self.track_name = track_name
        self.join_race()
        self.msg_loop()

    def on_join(self, data):
        debug("Joined")
        self.ping()

    def on_join_race(self, data):
        debug("Joined race")
        self.ping()

    def on_game_start(self, data):
        debug("Race started")
        self.ping()

    def on_car_positions(self, data):
        car = find_car(data, self.name)
        if car is None:
            debug("Car is None")
            self.ping()
            return
        pp = car['piecePosition']
        piece_index = pp['pieceIndex']
        current_piece = self.pieces[piece_index]
        in_piece = pp['inPieceDistance']
        lap = pp['lap']
        debug("lap: %d, piece #%d: %s, in_distance: %s" % (lap,
                                                           piece_index,
                                                           current_piece,
                                                           in_piece))
        if 'switch' in current_piece and not self.switched_already:
            self.switched_already = True
            self.switch_lane('right')
            return

        distance, next_turn = find_next_turn(self.pieces,
                                             piece_index,
                                             in_piece)
        debug("Next turn in %s" % (distance))

        a,b,c = self.drift_params
        d,e,f = self.slow_down_params

        if distance == 0:
            # crash angle is 60
            debug("Angle is %s" % car['angle'])
            if in_piece > arc_len(current_piece['radius'], current_piece['angle']) * 0.75:
                debug("past-center angle")
                th = 0.7
            else:
                th = a + ((b - abs(car['angle'])) / b) * c
        elif 0 < distance < e:
            th = d + ((distance / e) * f)
        else:
            th = self.safe_distance_throttle
        debug("setting throttle to %s" % th)
        self.throttle(th)

    def on_crash(self, data):
        debug("*"*80)
        debug("CRASH")
        debug("*"*80)
        sys.exit(0)
        self.ping()

    def on_game_end(self, data):
        debug("Race ended")
        self.ping()

    def on_game_init(self, data):
        debug("Game init")
        self.pieces = data['race']['track']['pieces']
        self.track_len = lap_len(self.pieces)
        debug("Track length is %s units" % self.track_len)
        self.lanes = sorted(data['race']['track']['lanes'], key=lambda x: x['distanceFromCenter'])
        self.ping()

    def on_error(self, data):
        debug("Error: {0}".format(data))
        self.ping()

    def on_spawn(self, data):
        debug("Spawned %s" % data['name'])

    def on_lap_finished(self, data):
        ticks_per_lap = data['lapTime']['ticks']
        debug("Lap time: %s" % ticks_per_lap)
        debug("Average speed: %s units/tick" % (self.track_len / ticks_per_lap))

    def on_disqualified(self, data):
        pass

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'dnf': self.on_disqualified,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                debug("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
