import json
import socket
import sys
import math
import random
import traceback
import re 
import time


finland = {
    'race': {
        'track': {
            'pieces': [{'length': 100.0},
                       {'length': 100.0},
                       {'length': 100.0},
                       {'length': 100.0, 'switch': True},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 22.5, 'switch': True, 'radius': 200},
                       {'length': 100.0},
                       {'length': 100.0},
                       {'angle': -22.5, 'radius': 200},
                       {'length': 100.0},
                       {'length': 100.0, 'switch': True},
                       {'angle': -45.0, 'radius': 100},
                       {'angle': -45.0, 'radius': 100},
                       {'angle': -45.0, 'radius': 100},
                       {'angle': -45.0, 'radius': 100},
                       {'length': 100.0, 'switch': True},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 22.5, 'radius': 200},
                       {'angle': -22.5, 'radius': 200},
                       {'length': 100.0, 'switch': True},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'length': 62.0},
                       {'angle': -45.0, 'switch': True, 'radius': 100},
                       {'angle': -45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'angle': 45.0, 'radius': 100},
                       {'length': 100.0, 'switch': True},
                       {'length': 100.0},
                       {'length': 100.0},
                       {'length': 100.0},
                       {'length': 90.0}],
            'lanes': [{'index': 0, 'distanceFromCenter': -10},
                      {'index': 1, 'distanceFromCenter': 10}],
            'name': 'Keimola',
            'id': 'keimola',
            'startingPoint': {
                'angle': 90.0,
                'position': {'y': -44.0, 'x': -300.0}
            }
		},
		'cars': [
                    {'id': {'color': 'red', 'name': 'molotov'},
                     'dimensions': {'length': 40.0,
                                    'width': 20.0,
                                    'guideFlagPosition': 10.0}
                 }],
		'raceSession': {'quickRace': True,
                                'laps': 3,
                                'maxLapTimeMs': 60000}
    }
}

carpos = [{
    'id': {
        'color': 'red',
        'name': 'molotov'
    },
    'angle': 0.0,
    'piecePosition': {
        'lane': {
            'startLaneIndex': 0,
            'endLaneIndex': 0
        },
        'inPieceDistance': 18.563953079998146,
        'pieceIndex': 0,
        'lap': 0
     }
}]


def find_car(cars, name):
    for car in cars:
        if car['id']['name'] == name:
            return car
    return None

def find_lane(lanes, index):
    for lane in lanes:
        if lane['index'] == index:
            return lane
    return None

def arc_len(radius, angle):
    return 2 * math.pi * radius * (abs(angle) / 360.0)

def lap_len(pieces):
    sum = 0
    for p in pieces:
        if 'angle' in p:
            sum += arc_len(p['radius'], p['angle'])
        else:
            sum += p['length']
    return sum

def debug(s):
    print(s)
    #pass

def error(s):
    print(s)

def warning(s):
    print(s)

def info(s):
    print(s)
    #pass

def piece_len(piece):
    if 'angle' in piece:
        return arc_len(piece['radius'], piece['angle'])
    else:
        return piece['length']

def distance_on_track(pieces, piece_index, in_piece_distance):
    return sum(piece_len(pieces[i]) for i in range(piece_index)) + in_piece_distance

# STATE is a distance from lap start to current car position
def make_state(in_lap_distance):
    return round(in_lap_distance)

# ACTIONS are (throttle 0.15, throttle 0.20, throttle, 0.25 and so on)
def actions():
    return list(map(lambda x: x / 20.0, range(3, 21)))

# REWARDS
def get_reward(old_state, new_state, action=0, angle=0):
    old_in_track = old_state
    new_in_track = new_state
    if old_in_track == new_in_track:
        return 0
    else:
        dang = abs(abs(angle) - 50)
        debug("Distance to angle safety limit is %s" % dang)
        return abs(new_in_track - old_in_track) * 10 + (action*10)

class QBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.track_name = ""
		
        self.pieces = []

        self.Q = None

        self.old_state = None
        self.old_action = None
        self.learning_factor = 1
        self.discount_factor = 0.8

        self.crash_count = 0
        self.crashed_right_now = False
        self.crash_rewarded = False

        self.logfile = open('car-%s.log' % time.ctime(), 'w')

    def log(self, msg):
        self.logfile.write(time.ctime() + ": " + msg + "\n")

    def load_q_table(self):
        info("Load Q table")
        filename = 'Q-%s.table' % self.track_name
        try:
            with open(filename) as f:
                self.Q = {}
                for s in f.readlines():
                    vals = re.findall(r"(\d+(?:\.\d+(?:e\-\d+)?)?)", s)
                    in_track, action, q = list(map(float, vals))
                    state = make_state(in_track)
                    self.Q[(state, action)] = q
            info("Loaded table with %d entries" % len(self.Q))
        except IOError:
            error("Can't load table: file not exists. Will initialize fresh table later (when `gameInit` comes).")
            self.Q = None

    def save_q_table(self):
        info("Save Q table")
        if self.Q is None:
            warning("Can't save empty Q table")
            return
        filename = 'Q-%s.table' % self.track_name
        with open(filename, 'w') as f:
            for state_action, q in self.Q.items():
                f.write("%s=%s\n" % (state_action, q))
        info("Table saved")

    def init_q_table(self):
        self.Q = {}
        info("Init Q table")
        for dist in range(int(self.track_len + 100)):
            state = make_state(dist)
            for action in actions():
                # if 0.5 < action <= 0.9: # biased initial Q table is bad, mkay?
                #     self.Q[(state, action)] = random.uniform(0.5, 1)
                # else:
				self.Q[(state, action)] = random.uniform(0, 1)
        info("Created Q table of size %d" % len(self.Q))
        self.save_q_table()

    def init_fancy_q_table(self):
        self.Q = {}
        info("Init Q table")
        for dist in range(int(self.track_len + 100)):
            state = make_state(dist)
            for action in actions():
                # if 0.5 < action <= 0.9: # biased initial Q table is bad, mkay?
                #     self.Q[(state, action)] = random.uniform(0.5, 1)
                # else:
                if action < 1:
                    self.Q[(state, action)] = random.uniform(0, 0.999999)
                else:
                    self.Q[(state, action)] = 1
        info("Created Q table of size %d" % len(self.Q))
        self.save_q_table()

    def max_q_old(self, current_state):
        for_state = [q
                     for (state, action), q in self.Q.items()
                     if state == current_state]
        debug("max: There are %d qs for current state" % len(for_state))
        if len(for_state) == 0:
            # happens after the end of the race
            error("max: Can't find state %s" % str(current_state))
            return 0
        return max(for_state)

    def max_q(self, state):
        max_q = -100000 # no time for infinity
        for action in actions():
            local_q = self.Q.get((state, action))
            if local_q is None:
                error("max: Can't find state %s" % str(state))
                return max_q
            if local_q > max_q:
                max_q = local_q
        return max_q

    def argmax_q_old(self, current_state):
        for_state = [(q, action, state)
                     for (state, action), q in self.Q.items()
                     if state == current_state]
        debug("argmax: There are %d actions for current state" % len(for_state))
        if len(for_state) == 0:
            error("argmax: Can't find state %s" % str(current_state))
            return for_state[0][1]
        return max(for_state)[1]

    def argmax_q(self, state):
        max_action = actions()[0]
        for action in actions():
            local_q = self.Q.get((state, action))
            max_q = self.Q.get((state, max_action))
            if local_q is None:
                error("argmax: Can't find state %s" % str(state))
                return 0
            if local_q > max_q:
                max_action = action
        return max_action

    def update_q(self, old_state, new_state, action, reward):
        if (old_state, action) not in self.Q:
            error("Error: table doesn't know state (%s, %s)" % (old_state, action))
        old = self.Q[(old_state, action)]
        self.Q[(old_state, action)] = self.Q[(old_state, action)] + self.learning_factor * (reward + self.discount_factor * self.max_q(new_state) - self.Q[(old_state, action)])
        new = self.Q[(old_state, action)]
        debug("Update Q: old Q(s,a) was %s, new one is %s" % (old, new))

    def decide(self, current_state):
        "returns action"
        return self.argmax_q(current_state)

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def switch_lane(self, direction):
        norm = "Left "if direction == "left" else "Right"
        return self.msg("switchLane", norm)

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self):
        return self.msg("joinRace", {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"trackName": self.track_name,
			"carCount": 1
		})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_race(self, track_name):
        self.track_name = track_name
        self.load_q_table()
        self.join_race()
        self.msg_loop()
        self.save_q_table()
        self.logfile.close()

    def on_join(self, data):
        info("Joined")
        self.ping()

    def on_join_race(self, data):
        info("Joined race")
        self.ping()

    def on_game_start(self, data):
        info("Race started")
        self.ping()

    def on_car_positions(self, data):
        # *observe*
        # form new state -> new_state
        # get reward for new state
        # *update*
        # update Q table entry for previous state and action
        # *decide*
        # select action that maximizes the Q table for current state
        # record previous state -> self.old_state
        car = find_car(data, self.name)
        if car is None:
            error("Can't find car in carPositions")
            self.ping()
            return
        pp = car['piecePosition']
        piece_index = pp['pieceIndex']
        current_piece = self.pieces[piece_index]
        in_piece = pp['inPieceDistance']
        lap = pp['lap']
        dist = distance_on_track(self.pieces,
                                 piece_index,
                                 in_piece)

        new_state = make_state(distance_on_track(self.pieces,
                                                 piece_index,
                                                 in_piece))

        if self.crashed_right_now:
            if not self.crash_rewarded:
                reward = -100
                debug("Reward for crash is %s" % reward)
                self.update_q(self.old_state, new_state, self.old_action, reward)
                self.crash_rewarded = True
            self.ping()
            return

        if 'angle' in car:
            debug("Drifting on angle %s" % car['angle'])

        info("lap: %d, in_lap: %s%%, piece #%d, in_distance: %s" % (lap,
                                                                    int(round((dist / self.track_len) * 100)),
                                                                    piece_index,
                                                                    in_piece))
        if self.old_state is None:
            action = 1.0
        else:
            reward = get_reward(self.old_state,
                                new_state,
                                action=self.old_action,
                                angle=car.get('angle', 0))
            debug("Reward for progress is %s" % reward)
            self.update_q(self.old_state, new_state, self.old_action, reward)
            action = self.decide(new_state)

        info("settings throttle to %s" % action)
        self.log("lap: %d, in_lap: %s%%, piece #%d, in_distance: %s :: throttle %s" %
				 (lap,
				  int(round((dist / self.track_len) * 100)),
				  piece_index,
				  in_piece,
				  action))

        self.throttle(action)
        self.old_state = new_state
        self.old_action = action

    def on_crash(self, data):
        self.crashed_right_now = True
        self.crash_rewarded = False
        self.crash_count += 1
        warning("*"*40)
        warning("CRASH")
        warning("*"*40)
        self.log("CRASH")
        self.ping()

    def on_game_end(self, data):
        info("Race ended")
        info("Good lord, you experienced %d crashes." % self.crash_count)
        self.log("Good lord, you experienced %d crashes." % self.crash_count)
        self.ping()

    def on_game_init(self, data):
        info("Game init")
        self.log("Track name is %s" % data['race']['track']['name'])
        self.pieces = data['race']['track']['pieces']
        self.laps = data['race']['raceSession']['laps']
        self.track_len = lap_len(self.pieces)
        warning("Track length is %s units" % self.track_len)
        self.log("Track length is %s units" % self.track_len)
        self.lanes = sorted(data['race']['track']['lanes'], key=lambda x: x['distanceFromCenter'])
        if self.Q is None:
            self.init_fancy_q_table()
        self.ping()

    def on_error(self, data):
        error("Error: {0}".format(data))
        self.ping()

    def on_spawn(self, data):
        self.crashed_right_now = False
        warning("Spawned %s" % data['name'])
        self.log("Spawned %s" % data['name'])

    def on_lap_finished(self, data):
        ticks_per_lap = data['lapTime']['ticks']
        warning("Lap time: %s" % ticks_per_lap)
        warning("Average speed: %s units/tick" % (self.track_len / float(ticks_per_lap)))
        self.log("Lap time: %s" % ticks_per_lap)
        self.log("Average speed: %s units/tick" % (self.track_len / float(ticks_per_lap)))

    def on_disqualified(self, data):
        pass

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'dnf': self.on_disqualified,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                warning("Got unhandled {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        debug("Connecting with parameters:")
        debug("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = QBot(s, name, key)
        bot.run_race("usa")

# this one seems to work
# but i don't really know how fast it will learn the track

# ideas:
# include speed in state set
# include angle and lane in state set
# BAD IDEA: reduce state set by setting track interval to 5 or even 10 instead of 1 unit
# DONE: reduce state set by using only one lap
# insert more throttle values into the state set (step 0.025, for example)
# radically reduce state space (1 track segment - 1 state)
# q learning only for curves
