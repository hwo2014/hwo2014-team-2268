class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
		
        self.pieces = []

        self.switched_already = False
        self.last_switch_piece = -1

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(bytes(msg + "\n", "ascii"))

    def switch_lane(self, direction):
        norm = "Left "if direction == "left" else "Right"
        return self.msg("switchLane", norm)

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self, track_name):
        return self.msg("joinRace", {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"trackName": track_name,
			"carCount": 1
		})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_race(self, track_name):
        self.join_race(track_name)
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_join_race(self, data):
        print("Joined race")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        car = find_car(data, self.name)
        if car is None:
            print("Car is None")
            self.ping()
            return
        pp = car['piecePosition']
        piece_index = pp['pieceIndex']
        current_piece = self.pieces[piece_index]
        in_piece = pp['inPieceDistance']
        lap = pp['lap']
        print("lap: %d, piece #%d: %s, in_distance: %s" % (lap,
                                                           piece_index,
                                                           current_piece,
                                                           in_piece))

        if piece_index != self.last_switch_piece:
            self.switched_already = False

        distance, next_turn = find_next_dangerous_turn(self.pieces,
                                                       piece_index,
                                                       in_piece)
        print("Next turn in %s" % (distance))

        if distance == 0:
            # crash angle is 60
            th = 0.3 + ((55 - abs(car['angle'])) / (55 * 4))
            print("Angle is %s" % car['angle'])
        elif 0 < distance < 70:
            th = 0.25 + ((70 - distance) / (70 * 4))
        else:
            th = 0.9
        print("setting throttle to %s" % th)
        self.throttle(th)

    def on_crash(self, data):
        print("*"*80)
        print("CRASH")
        print("*"*80)
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        self.pieces = data['race']['track']['pieces']
        self.track_len = lap_len(self.pieces)
        print("Track length is %s units" % self.track_len)
        self.lanes = sorted(data['race']['track']['lanes'], key=lambda x: x['distanceFromCenter'])
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_spawn(self, data):
        print("Spawned %s" % data['name'])

    def on_lap_finished(self, data):
        ticks_per_lap = data['lapTime']['ticks']
        print("Lap time: %s" % ticks_per_lap)
        print("Average speed: %s units/tick" % (self.track_len / ticks_per_lap))

    def on_disqualified(self, data):
        pass

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'dnf': self.on_disqualified,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()
