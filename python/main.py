import json
import socket
import sys
import math
import random
import traceback
import re 
import time


def find_car(cars, name):
    for car in cars:
        if car['id']['name'] == name:
            return car
    return None

def find_lane(lanes, index):
    for lane in lanes:
        if lane['index'] == index:
            return lane
    return None

def arc_len(radius, angle):
    return 2.0 * math.pi * radius * (abs(angle) / 360.0)

def lap_len(pieces):
    "does not consider lanes"
    # TODO: fix
    sum = 0.0
    for p in pieces:
        if 'angle' in p:
            sum += arc_len(p['radius'], p['angle'])
        else:
            sum += p['length']
    return sum

def debug(s):
    print(s)
    #pass

def error(s):
    print(s)

def warning(s):
    print(s)

def info(s):
    print(s)
    #pass

def piece_len(piece, lane_radius):
    "does not consider lanes"
    if 'angle' in piece:
        return arc_len(piece['radius'] - lane_radius, piece['angle'])
    else:
        return piece['length']

def distance_on_track(pieces, lane, piece_index, in_piece_distance):
    return sum(piece_len(pieces[i], lane['distanceFromCenter']) for i in range(piece_index)) + in_piece_distance

# Z is a mapping from states to optimal throttles.
# States are segments of track piece.
# Every piece has K equal segments.
K = 3
# By default, ultimate optimal throttle is 1.0
DEFAULT_THROTTLE = 0.95
# If crash occurred, throttle for the last state gets decreased by certain step.
THROTTLE_DECREASE_STEP = 0.15
# There is also a limit in decreasing
THROTTLE_DECREASE_LIMIT = 0.2
# angle limit
SAFE_ANGLE_LIMIT = 40
# perform number of consequent decreases per crash
CONSEQUENT_DECREASES_COUNT = 10

def make_state(piece_index, in_piece_segment):
    return (piece_index, in_piece_segment)

class ZBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        self.track_name = ""

        self.pieces = []
        self.lanes = []

        self.Z = None

        self.last_state = None

        self.last_distance = 0

        self.crash_count = 0
        self.crashed_right_now = False
        self.crash_rewarded = False

        self.logfile = open('car-%s.log' % time.ctime(), 'w')

    def log(self, msg):
        self.logfile.write(time.ctime() + ": " + msg + "\n")

    def load_z_table(self):
        info("Load Z table")
        filename = 'Z-%s.table' % self.track_name
        try:
            with open(filename) as f:
                self.Z = {}
                for s in f.readlines():
                    if s.startswith("#"):
	                    continue
                    vals = re.findall(r"(\d+(?:\.\d+(?:e\-\d+)?)?)", s)
                    piece_index, in_piece_segment, throttle = list(map(float, vals))
                    state = make_state(piece_index, in_piece_segment)
                    self.Z[state] = throttle
            info("Loaded table with %d entries" % len(self.Z))
        except IOError:
            error("Can't load table: file not exists. Will initialize fresh table later (when `gameInit` comes).")
            self.Z = None

    def save_z_table(self):
        info("Save Z table")
        if self.Z is None:
            warning("Can't save empty Z table")
            return
        filename = 'Z-%s.table' % self.track_name
        with open(filename, 'w') as f:
            for state, throttle in self.Z.items():
                f.write("%s=%s\n" % (state, throttle))
        info("Table saved")

    def init_z_table(self):
        self.Z = {}
        info("Init Z table")
        for index, piece in enumerate(self.pieces):
            for k in range(K):
                state = make_state(index, k)
                self.Z[state] = DEFAULT_THROTTLE
        info("Created Z table of size %d" % len(self.Z))
        self.save_z_table()

    def decrease_z(self, old_state):
        value = self.Z[old_state]
        if value > THROTTLE_DECREASE_LIMIT:
            self.Z[old_state] = value - THROTTLE_DECREASE_STEP
        else:
            # take previous state
            # decrease it
            piece_index, segment = old_state
            if segment > 0:
                self.decrease_z(make_state(piece_index, segment - 1))
            else:
                if piece_index > 0:
                    self.decrease_z(make_state(piece_index - 1, K - 1))
                else:
                    error("BAD"*20)
                    sys.exit(0)

    def decide_z(self, state):
        return self.Z[state]

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def switch_lane(self, direction):
        norm = "Left "if direction == "left" else "Right"
        return self.msg("switchLane", norm)

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def join_race(self):
        return self.msg("joinRace", {
			"botId": {
				"name": self.name,
				"key": self.key
			},
			"trackName": self.track_name,
			"carCount": 1
		})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def run_race(self, track_name):
        self.track_name = track_name
        self.load_z_table()
        self.join_race()
        self.msg_loop()
        self.save_z_table()
        self.logfile.close()

    def on_join(self, data):
        info("Joined")
        self.ping()

    def on_join_race(self, data):
        info("Joined race")
        self.ping()

    def on_game_start(self, data):
        info("Race started")
        self.ping()

    def on_car_positions(self, data):
        car = find_car(data, self.name)
        if car is None:
            error("Can't find car in carPositions")
            return self.ping()
        pp = car['piecePosition']
        piece_index = pp['pieceIndex']
        current_piece = self.pieces[piece_index]
        in_piece = pp['inPieceDistance']
        lap = pp['lap']
        lane_index = pp['lane']['startLaneIndex']
        lane = find_lane(self.lanes, lane_index)
        dist = distance_on_track(self.pieces,
                                 lane,
                                 piece_index,
                                 in_piece)

        speed = dist - self.last_distance
        self.last_distance = dist

        if self.crashed_right_now:
            if not self.crash_rewarded:
                for i in range(CONSEQUENT_DECREASES_COUNT):
                    self.decrease_z(self.last_state)
                self.crash_rewarded = True
                # TOHAVE: save Z table and sys.exit
                self.save_z_table()
                #sys.exit(1)
            return self.ping()

        if 'angle' in car:
            debug("Drifting on angle %s" % car['angle'])
            if abs(car['angle']) > SAFE_ANGLE_LIMIT:
                debug("decreasing Z because of safe angle limit")
                self.decrease_z(self.last_state)
                self.save_z_table()
            
        info("="*40)
        info("lap: %d, in_lap: %s%%, piece #%d, in_distance: %s" % (lap,
                                                                    (dist / self.track_len) * 100.0,
                                                                    piece_index,
                                                                    in_piece))
        print("Momentum speed is %s" % speed)

        # figure out current state
        piece_length = piece_len(current_piece, lane['distanceFromCenter'])
        segment = int(in_piece / piece_length * K)
        current_state = make_state(piece_index, segment)

        throttle = self.decide_z(current_state)
        info("setting throttle to %s" % throttle)
        self.log("lap: %d, in_lap: %s%%, piece #%d, in_distance: %s, throttle: %s" %
				 (lap,
				  dist / self.track_len * 100.0,
				  piece_index,
				  in_piece,
				  throttle))

        self.throttle(throttle)
        self.last_state = current_state

    def on_crash(self, data):
        self.crashed_right_now = True
        self.crash_rewarded = False
        self.crash_count += 1
        warning("*"*40)
        warning("CRASH")
        warning("*"*40)
        self.log("CRASH")
        self.ping()

    def on_game_end(self, data):
        info("Race ended")
        info("Good lord, you experienced %d crashes." % self.crash_count)
        self.log("Good lord, you experienced %d crashes." % self.crash_count)
        self.ping()

    def on_game_init(self, data):
        info("Game init")
        self.log("Track name is %s" % data['race']['track']['name'])
        self.pieces = data['race']['track']['pieces']
        self.laps = data['race']['raceSession']['laps']
        self.track_len = lap_len(self.pieces)
        warning("Track length is %s units" % self.track_len)
        self.log("Track length is %s units" % self.track_len)
        self.lanes = sorted(data['race']['track']['lanes'], key=lambda x: x['distanceFromCenter'])
        if self.Z is None:
            self.init_z_table()
        self.ping()

    def on_error(self, data):
        error("Error: {0}".format(data))
        self.ping()

    def on_spawn(self, data):
        self.crashed_right_now = False
        warning("Spawned %s" % data['name'])
        self.log("Spawned %s" % data['name'])

    def on_lap_finished(self, data):
        lap_number = data['lapTime']['lap']
        ticks_per_lap = data['lapTime']['ticks']
        millis_per_lap = data['lapTime']['millis']
        warning("Lap #%d time: %s ticks, %s millis" % (lap_number, ticks_per_lap, millis_per_lap))
        warning("Average speed: %s units/tick" % (self.track_len / float(ticks_per_lap)))
        self.log("Lap #%d time: %s ticks, %s millis" % (lap_number, ticks_per_lap, millis_per_lap))
        self.log("Average speed: %s units/tick" % (self.track_len / float(ticks_per_lap)))

    def on_disqualified(self, data):
        pass

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'joinRace': self.on_join_race,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'spawn': self.on_spawn,
            'lapFinished': self.on_lap_finished,
            'dnf': self.on_disqualified,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                warning("Got unhandled {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        debug("Connecting with parameters:")
        debug("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = ZBot(s, name, key)
        bot.run_race("france")

# NOTES:
# * TODO poke with top-level constants (DECREASE_*, K and stuff)
#   decreasing THROTTLE_DECREASE_STEP slows learning (but in general leads to more optimal result)
#   decreasing THROTTLE_DECREASE_LIMIT does the same
# * DONE see how fast will that learning method converge
#   okay, for finland it took ~8 hours to converge to 8.20 seconds per lap. Good, but could be better.
# * TODO decrease initial values of throttle for angles (so the algorithm will converge faster)
# * create plots for throttle/speed/other stuff
# * use converged Z table as an input for Q table for learning lane switches and turbos
# * make more decreases per crash
# ** when crashed decrease throttle for last L segments, not only the last one
# ** when crashed decrease throttle for F future segments
# ** these ones can speed up learining a bit, but can lose optimality
# * TODO ZBot is strongly decreasing bot. How about a new kind of bot that is increasing its throttles.
